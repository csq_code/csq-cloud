import request from '@/utils/request'

// 查询博客列表
export function list(query) {
  return request({
    url: '/front/homeBlogList',
    method: 'get',
    params: query
  })
}

export function getBlogTypeOptions(){
	return request({
	  url: '/front/getBlogTypeOptions',
	  method: 'get'
	})
}

export function getArticleDetail(id) {
	return request({
	  url: '/front/getArticleDetail/' + id,
	  method: 'get'
	})
}

export function saveArticle(data) {
  return request({
    url: '/front/saveArticle',
    method: 'put',
    data: data
  })
}

export function delArticle(ids) {
  return request({
    url: '/front/delArticle/' + ids,
    method: 'delete'
  })
}

export function getArticleLabelDict() {
	return request({
	  url: '/front/getArticleLabelDict/',
	  method: 'get'
	})
}