import router from './router'
import { ElMessage } from 'element-plus'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

NProgress.configure({ showSpinner: false });

router.beforeEach((to, from, next) => {
	
})

router.afterEach(() => {
  NProgress.done()
})
