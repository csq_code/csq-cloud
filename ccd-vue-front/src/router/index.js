import { createWebHistory, createRouter } from "vue-router";

import Layout from '@/layout'

// 公共路由
export const constantRoutes = [
	{
	  path: "/:pathMatch(.*)*",
	  redirect: "/404",
	},
	{
	  path: "/404",
	  component: () => import("@/views/error/404.vue"),
	},
	{
	  path: "",
	  redirect: "/home",
	},
	{
	  path: "/music",
	  redirect: "/music/home",
	},
	{
	  path: '',
	  component: Layout,
	  children: [
	    {
	      path: '/home',
	      component: () => import('@/views/homePages/index.vue')
	    },
	    {
	      path: '/music',
	      component: () => import('@/views/musicPages/index.vue'),
		  children: [
			  {
				  path: '/music/home',
				  component: () => import('@/views/musicPages/pages/home.vue')
			  },
			  {
				  path: '/music/songs',
				  component: () => import('@/views/musicPages/pages/songs.vue')
			  },
		  ]
	    },
	    {
	      path: '/video',
	      component: () => import('@/views/videoPages/index.vue')
	    },
	    {
	      path: '/ai',
	      component: () => import('@/views/AIPages/index.vue')
	    },
		{
		  path: '/alg',
		  component: () => import('@/views/algPages/index.vue')
		},
	  ]
	}
]

const router = createRouter({
  history: createWebHistory(),
  routes: constantRoutes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { top: 0 }
    }
  },
});

export default router;