import { createApp } from 'vue'

import Cookies from 'js-cookie'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import locale from 'element-plus/es/locale/lang/zh-cn'

import App from './App.vue'
import router from './router/index.js'
import pinia from './store/index.js'

// svg图标
import elementIcons from '@/components/SvgIcon/svgicon'

//视频播放组件
import vue3videoPlay from 'vue3-video-play' // 引入组件
import 'vue3-video-play/dist/style.css' // 引入css

// import './permission' // permission control

import 'bytemd/dist/index.css';

const app = createApp(App)

app.use(router)
app.use(pinia)
app.use(elementIcons)
app.use(vue3videoPlay)

// 使用element-plus 并且设置全局的大小
app.use(ElementPlus, {
  locale: locale,
  // 支持 large、default、small
  size: Cookies.get('size') || 'default'
})

app.mount('#app')
