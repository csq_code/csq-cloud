import request from '@/api/request'

import { getBaseURL, get, post, deletes } from "@/api/request";

const HttpManager = {
	// 获取图片信息
	attachImageUrl: (url) => url ? `${getBaseURL()}/file/statics${url}` : "https://cube.elemecdn.com/e/fd/0fc7d20532fdaf769a25683617711png.png",
	// 获取全部歌单
	getSongList: () => get("/music/songList"),
}

export { HttpManager };