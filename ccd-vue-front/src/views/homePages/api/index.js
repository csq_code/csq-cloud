import request from '@/api/request'

import { getBaseURL, get, post, deletes } from "@/api/request";

const HttpManager = {
	// 获取资源信息
	attachResourcesUrl: (url) => (url && url.indexOf("http") < 0) ? 
		`${getBaseURL()}/file/statics/${url}?_t=${new Date()}` : 
		url,
	
	getBlogCardList: (pageNum, pageSize, searchText) => get("/front/homeBlogList?pageNum=" + pageNum + "&pageSize=" + pageSize + "&searchText=" + searchText),
}

export { HttpManager };