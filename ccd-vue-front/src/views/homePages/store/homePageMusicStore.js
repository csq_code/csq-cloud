import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useHomePageStore } from './homePageStore.js'

/**
 * 首页音乐控件 状态机
 */
export const useHomePageMusicStore = defineStore('homePageMusicStore', {
	state() {
		return  {
			duration: 10, // 音乐时长
			curTime: 0, // 当前音乐的播放位置
			changeTime: 0, // 指定播放时刻
			
			//==========音频可视化===========//
			//当前画布容器canvas
			curCanvas: null,
			//当前音频可视化画布
			curAud2DCtx: null,
			//当前音频处理器上下文
			curAudCtx: null,
			//音源节点
			audioSource: null,
			//分析器节点
			analyser: null,
			//分析器分析结果数组
			dataArray: null,
			//当前动画id
			animationId: null,
			//==========音频可视化===========//
			
			//==========歌词解析，音乐组件的 lyric 设置为歌词===========//
			songLyc: "",
			//==========歌词解析===========//
			
			
		}
	},
	getters: {
		getDuration() { return this.duration },
		getCurTime() { return this.curTime },
		getChangeTime() { return this.changeTime },
		
		getCurCanvas(){ return this.curCanvas },
		getCurAud2DCtx(){ return this.curAud2DCtx },
		getCurAudCtx(){ return this.curAudCtx },
		getAudioSource(){ return this.audioSource },
		getAnalyser(){ return this.analyser },
		getDataArray(){ return this.dataArray },
		
		getSongLyc() { return this.songLyc },
	},
	actions: {
		setDuration(duration)  { this.duration = duration; },
		setCurTime(curTime)  { this.curTime = curTime; },
		setChangeTime(changeTime)  { this.changeTime = changeTime; },
		
		setCurCanvas(value){ this.curCanvas = value },
		setCurAud2DCtx(value){ this.curAud2DCtx = value },
		setCurAudCtx(value){ this.curAudCtx = value },
		setAudioSource(value){ this.audioSource = value },
		setAnalyser(value){ this.analyser = value; this.analyser.fftSize = 512; },
		setDataArray(value){ this.dataArray = new Uint8Array(this.analyser.frequencyBinCount); },
		
		setSongLyc(value) { this.songLyc = value },
		
		//音频处理上下文相关数据全部初始化
		initCurAudCtx(curCanvas, curAud2DCtx, analyser, dataArray){
			this.setCurCanvas(curCanvas)
			this.setCurAud2DCtx(curAud2DCtx)
			this.setAnalyser(analyser)
			this.setDataArray(dataArray)
		},
		
		//传入canvas容器 和 音源容器
		initCvs(cvs, audio){
			this.curCanvas = cvs;
			//在canvas容器里创建画布
			this.curAud2DCtx = cvs.getContext('2d');
			//创建音频处理器上下文
			this.curAudCtx = new AudioContext();
			this.audioSource = this.curAudCtx.createMediaElementSource(audio); //创建音频源节点
			this.analyser = this.curAudCtx.createAnalyser();
			this.analyser.fftSize = 512;
			//创建数组，用于接受分析器节点分析的数据
			this.dataArray = new Uint8Array(this.analyser.frequencyBinCount);
			
			this.audioSource.connect(this.analyser);
			this.analyser.connect(this.curAudCtx.destination);
		},
		
		startDrawAudioScope(){
			// 清除之前的动画（如果有的话）
			if (this.animationId !== null) {
				cancelAnimationFrame(this.animationId);  
				this.animationId = null;  
			}
			
			// 在这里更新动画状态
			this.animationId = requestAnimationFrame(this.startDrawAudioScope);
			
			const{width, height} = this.curCanvas;
			this.curAud2DCtx.clearRect(0, 0, width, height)
			//让分析器节点分析数据到数组中
			this.analyser.getByteFrequencyData(this.dataArray)
			const len = this.dataArray.length / 2.5;
			const barWidth = width / len;
			
			for (let i = 0; i < len; i++) {
				const data = this.dataArray[i];
				const barHeight = data / 255 * height;
				const x = i * barWidth;
				const y = height - barHeight;
				this.curAud2DCtx.fillStyle = "red";
				this.curAud2DCtx.fillRect(x, y, barWidth - 2, barHeight);
			}
		},
		
		stopAnimation(getItemIndex) {
			const storeH = useHomePageStore()
			if(getItemIndex != storeH.getCurSelItemIndex){//如果获取到的项目序号 不等于当前序号，说明切换了项目, 那么不执行动画停止操作
				return
			}
		    if (this.animationId !== null) {  
				// 停止动画  
				cancelAnimationFrame(this.animationId);  
				this.animationId = null; // 重置ID，防止后续误操作
			}  
		},
		
		/**
		 * 解析歌词字符串
		 * 得到一个歌词对象数组
		 * { time: 开始时间, words: 歌词内容 }
		 */
		parseLrc(){
			if(!this.songLyc){
				return [{"time": "0.00", words: "暂无歌词"}]
			}
			let lines = this.songLyc.split('\n');
			let result = [];
			for (let i = 0; i < lines.length; i++) {
				let str = lines[i];
				let obj = {
					time: this.praseTime(str.split(']')[0].substring(1)),
					words: str.split(']')[1]
				};
				result.push(obj);
			}
			return result;
			
		},
		
		praseTime(timeStr){
			let parts = timeStr.split(':');
			return (+parts[0] * 60 + +parts[1]).toFixed(2);
		},
		
	}
})