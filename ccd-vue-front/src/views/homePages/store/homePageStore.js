import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

/**
 * 首页控件 状态机
 */
export const useHomePageStore = defineStore('homePageStore', {
	state() {
		return  {
			//当前列表选中的项目itemIndex
			curSelItemIndex: 0,
		}
	},
	getters: {
		getCurSelItemIndex(){ return this.curSelItemIndex },
	},
	actions: {
		setCurSelItemIndex(index){ this.curSelItemIndex = index },
	}
})