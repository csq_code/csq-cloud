import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

/**
 * 首页视频控件 状态机
 */
export const useHomePageVideoStore = defineStore('homePageVideoStore', {
	state() {
		return  {
			
		}
	},
	getters: {
		
	},
	actions: {
		
	}
})