import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useCounterStore = defineStore('counter', {
	state(){
		return{
			num: 1
		}
	},
	getters:{
		doubleNum(){
			return this.num * 2
		}
	},
	actions:{
		inc(){
			this.num++
		}
	}
})