import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

/**
 * 全局控件 状态机
 */
export const useGlobalStore = defineStore('globalStore', {
	state() {
		return  {
			//是否展示左侧目录条
			showNavBar: true,
			//是否展示网页横向滚动进度条
			showScrollContent: true,
			//是否匹配了页面高度
			hasAssignHeight: false,
			//是否PC端
			isPc: true,
		}
	},
	getters: {
		getShowNavBar(){ return this.showNavBar },
		getShowScrollContent(){ return this.showScrollContent },
		getHasAssignHeight(){ return this.hasAssignHeight },
		getIsPc(){ return this.isPc },
	},
	actions: {
		setShowNavBar(value){ this.showNavBar = value },
		setShowScrollContent(value){ this.showScrollContent = value },
		setHasAssignHeight(value){ this.hasAssignHeight = value },
		setIsPc(value){ this.isPc = value },
	}
})