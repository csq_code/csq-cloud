package com.ccd.article;

import com.ccd.common.security.annotation.EnableCustomConfig;
import com.ccd.common.security.annotation.EnableRyFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableCustomConfig
@EnableRyFeignClients
@SpringBootApplication
public class CcdArticleApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(CcdArticleApplication.class, args);
    }
}
