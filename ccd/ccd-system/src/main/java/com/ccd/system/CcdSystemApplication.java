package com.ccd.system;

import com.ccd.common.security.annotation.EnableCustomConfig;
import com.ccd.common.security.annotation.EnableRyFeignClients;
import com.ccd.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class CcdSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(CcdSystemApplication.class, args);
    }

}
