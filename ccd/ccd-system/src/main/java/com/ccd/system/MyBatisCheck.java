package com.ccd.system;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MyBatisCheck implements CommandLineRunner {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public void run(String... args) throws Exception {
        Configuration configuration = sqlSessionFactory.getConfiguration();
        configuration.getMappedStatements().forEach(mappedStatement -> {
            System.out.println("Mapped Statement: " + mappedStatement.getId());
        });
    }
}

