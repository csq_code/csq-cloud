package com.ccd.music.config;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * 项目启动时，打印扫描到的mapper信息
 * 起因是，resources下的mapper/music路径问题，如果直接新建mapper.music包，这时候mapper.music只是一个文件夹，并不是mapper/music一个路径，
 * 如果新建mapper/music路径，但这个路径下没有子路径了，那在idea中显示就会和mapper.music显示一样，造成混淆
 */
@Component
public class MyBatisCheck implements CommandLineRunner {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public void run(String... args) throws Exception {
        Configuration configuration = sqlSessionFactory.getConfiguration();
        configuration.getMappedStatements().forEach(mappedStatement -> {
            System.out.println("Mapped Statement: " + mappedStatement.getId());
        });
    }
}

