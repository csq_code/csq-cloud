package com.ccd.music.mapper;

import com.ccd.music.model.domain.SongList;

import java.util.List;

public interface SongListMapper {

    public List<SongList> selectAllSong();

}
