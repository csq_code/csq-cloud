package com.ccd.music.service;

import com.ccd.music.common.response.R;
import com.ccd.music.model.domain.SongList;

import java.util.List;

public interface SongListService {

    List<SongList> allSongList();
}
