package com.ccd.music.service.impl;

import com.ccd.music.common.response.R;
import com.ccd.music.mapper.SongListMapper;
import com.ccd.music.model.domain.SongList;
import com.ccd.music.service.SongListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SongListServiceImpl implements SongListService {

    @Autowired
    private SongListMapper songListMapper;

    @Override
    public List<SongList> allSongList() {
        List<SongList> ls = songListMapper.selectAllSong();
        return ls;
    }
}
