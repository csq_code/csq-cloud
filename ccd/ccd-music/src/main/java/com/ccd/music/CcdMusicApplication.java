package com.ccd.music;

import com.ccd.common.security.annotation.EnableCustomConfig;
import com.ccd.common.security.annotation.EnableRyFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 音乐模块调用
 * @author ccd
 */
@EnableCustomConfig
@EnableRyFeignClients
@SpringBootApplication
public class CcdMusicApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(CcdMusicApplication.class, args);
    }
}
