package com.ccd.music.controller;

import com.ccd.music.common.response.R;
import com.ccd.music.model.domain.SongList;
import com.ccd.music.service.SongListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SongListController {

    @Autowired
    private SongListService songListService;

    /**
     * 获取所有歌单信息
     * @return
     */
    @GetMapping("/songList")
    public R allSongList() {
        return R.success(null, songListService.allSongList());
    }
}
