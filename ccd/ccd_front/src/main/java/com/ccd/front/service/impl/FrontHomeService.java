package com.ccd.front.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ccd.common.core.utils.StringUtils;
import com.ccd.common.core.web.page.PageDomain;
import com.ccd.front.constnt.CardTypeConstants;
import com.ccd.front.mapper.FrontHomeMapper;
import com.ccd.front.model.domain.*;
import com.ccd.front.service.IFrontHomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class FrontHomeService implements IFrontHomeService {

    @Autowired
    private FrontHomeMapper frontHomeMapper;

    @Override
    public List<Dict> getBlogTypeOptions() {
        return frontHomeMapper.getBlogTypeOptions();
    }

    @Override
    public Page<BlogCard> getHomeBlogList(PageDomain pageDomain) {

        Page<BlogCard> page = new Page<>(pageDomain.getPageNum(), pageDomain.getPageSize());
        frontHomeMapper.getHomeBlogList(page, pageDomain.getSearchText());

        for(BlogCard e : page.getRecords()){
            //音乐卡片
            if(e.getCardType().equals(CardTypeConstants.MUSIC)){
                BlogMusicCard blogMusicCard = frontHomeMapper.getBlogMusicCardById(e.getResourceId());
                e.setBolgContent(blogMusicCard);
            }
            //视频卡片
            if(e.getCardType().equals(CardTypeConstants.VIDEO)){
                BlogVideoCard blogVideoCard = frontHomeMapper.getBlogVideoCardById(e.getResourceId());
                e.setBolgContent(blogVideoCard);
            }
            //文章卡片
            if(e.getCardType().equals(CardTypeConstants.ARTICLE)){
                BlogArticleCard blogArticleCard = frontHomeMapper.getBlogArticleCardById(e.getResourceId());
                e.setBolgContent(blogArticleCard);
            }
        }
        return page;
    }

    @Override
    public BlogArticleCard getArticleDetail(int id) {
        BlogArticleCard blogArticleCard = frontHomeMapper.getArticleDetail(id);
        if(StringUtils.isNotEmpty(blogArticleCard.getLabelsStr())){
            blogArticleCard.setLabels(StringUtils.str2List(blogArticleCard.getLabelsStr().replaceAll("\\s+", "")));
        }
        return blogArticleCard;
    }

    @Override
    public void saveArticle(BlogArticleCard blogArticleCard) {
        if(blogArticleCard.getLabels() != null){
            blogArticleCard.setLabelsStr(blogArticleCard.getLabels().toString().replaceAll("\\s+", ""));
        }
        if(blogArticleCard.getId() != null){//更新
            frontHomeMapper.updArticle(blogArticleCard);
            frontHomeMapper.updArticleMain(blogArticleCard);
        }else{//插入
            frontHomeMapper.saveArticle(blogArticleCard);
            frontHomeMapper.saveArticleMain(blogArticleCard);
        }
    }

    @Override
    public void delArticle(int[] ids) {
        // 后面该逻辑删除
        frontHomeMapper.delArticle(ids);
        frontHomeMapper.delArticleMain(ids);
    }

    @Override
    public List<Map<String, Object>> getArticleLabelDict() {
        return frontHomeMapper.getArticleLabelDict();
    }

}
