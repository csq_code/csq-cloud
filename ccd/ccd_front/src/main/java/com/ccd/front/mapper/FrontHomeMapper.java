package com.ccd.front.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ccd.front.model.domain.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface FrontHomeMapper {

    IPage<BlogCard> getHomeBlogList(IPage<BlogCard> page, @Param("cond") String cond);


    BlogMusicCard getBlogMusicCardById(@Param("resourceId") int resourceId);

    BlogVideoCard getBlogVideoCardById(@Param("resourceId") int resourceId);

    BlogArticleCard getBlogArticleCardById(@Param("resourceId") int resourceId);

    List<Dict> getBlogTypeOptions();

    int saveArticle(BlogArticleCard blogArticleCard);

    void saveArticleMain(BlogArticleCard blogArticleCard);

    BlogArticleCard getArticleDetail(@Param("id") int id);

    void updArticle(BlogArticleCard blogArticleCard);

    void updArticleMain(BlogArticleCard blogArticleCard);

    void delArticle(int[] ids);

    void delArticleMain(int[] ids);

    List<Map<String, Object>> getArticleLabelDict();
}
