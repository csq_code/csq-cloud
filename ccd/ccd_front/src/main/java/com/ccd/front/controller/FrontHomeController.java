package com.ccd.front.controller;

import com.ccd.common.core.domain.R;
import com.ccd.common.core.web.page.PageDomain;
import com.ccd.front.model.domain.BlogArticleCard;
import com.ccd.front.model.domain.BlogCard;
import com.ccd.front.service.IFrontHomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class FrontHomeController {

    @Autowired
    private IFrontHomeService frontHomeService;

    /**
     * 获取博客类型字典
     * @return
     */
    @GetMapping("/getBlogTypeOptions")
    public R getBlogTypeOptions() {
        return R.ok(frontHomeService.getBlogTypeOptions());
    }

    /**
     * 首页获取发表的博客信息列表
     * @return
     */
    @GetMapping("/homeBlogList")
    public R getHomeBlogList(PageDomain pageDomain) {
        return R.ok(frontHomeService.getHomeBlogList(pageDomain));
    }

    /**
     * 查询文章详情
     * @param id
     * @return
     */
    @GetMapping("/getArticleDetail/{id}")
    public R getArticleDetail(@PathVariable int id) {
        return R.ok(frontHomeService.getArticleDetail(id));
    }

    /**
     * 查询文章标签字典
     * @return
     */
    @GetMapping("/getArticleLabelDict")
    public R getArticleLabelDict() {
        return R.ok(frontHomeService.getArticleLabelDict());
    }

    /**
     * 保存文章
     * @return
     */
    @PutMapping("/saveArticle")
    public R saveArticle(@RequestBody BlogArticleCard blogArticleCard) {

        frontHomeService.saveArticle(blogArticleCard);
        return R.ok();
    }

    /**
     * 删除文章
     * @return
     */
    @DeleteMapping ("/delArticle/{ids}")
    public R delArticle(@PathVariable int[] ids) {
        frontHomeService.delArticle(ids);
        return R.ok();
    }

}
