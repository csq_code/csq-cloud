package com.ccd.front.model.domain;

import lombok.Data;

@Data
public class BlogVideoCard {

    /**
     * 主键
     */
    private Integer id;

    private String videoName;
    private String videoAuthor;
    private String videoIntro;
    private String posterUrl;
    private String resourceUrl;
}
