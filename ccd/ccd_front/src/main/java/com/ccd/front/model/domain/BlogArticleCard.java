package com.ccd.front.model.domain;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class BlogArticleCard {

    /**
     * 主键
     */
    private Integer id;

    private String title;

    /**
     * 封面图片
     */
    private String picture;

    /**
     * 标签
     */
    private List<String> labels;

    /**
     * 标签数组字符串，前端获取时重转为数组
     */
    private String labelsStr;

    /**
     * 文章描述
     */
    private String description;
    private String content;
    private Timestamp createTime;
    private Timestamp updateTime;

    /**
     * 文章浏览数
     */
    private int views;
    /**
     * 文章字数
     */
    private int words;
    /**
     * 文章分类id
     */
    private int typeId;
    /**
     * 文章作者ID
     */
    private int userId;


}
