package com.ccd.front.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ccd.common.core.web.page.PageDomain;
import com.ccd.front.model.domain.BlogArticleCard;
import com.ccd.front.model.domain.BlogCard;
import com.ccd.front.model.domain.Dict;

import java.util.List;
import java.util.Map;

public interface IFrontHomeService {

    Page<BlogCard> getHomeBlogList(PageDomain pageDomain);

    List<Dict> getBlogTypeOptions();

    void saveArticle(BlogArticleCard blogArticleCard);

    BlogArticleCard getArticleDetail(int id);

    void delArticle(int[] ids);

    List<Map<String, Object>> getArticleLabelDict();
}
