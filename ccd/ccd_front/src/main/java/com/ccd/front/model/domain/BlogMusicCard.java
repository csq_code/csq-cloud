package com.ccd.front.model.domain;

import lombok.Data;

@Data
public class BlogMusicCard {

    /**
     * 主键
     */
    private Integer id;

    private String musicName;
    private String musicAuthor;
    private String lyric;
    private String picUrl;
    private String resourceUrl;
}
