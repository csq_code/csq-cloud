package com.ccd.front;

import com.ccd.common.security.annotation.EnableCustomConfig;
import com.ccd.common.security.annotation.EnableRyFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableCustomConfig
@EnableRyFeignClients
@SpringBootApplication
public class CcdFrontApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(CcdFrontApplication.class, args);
    }
}
