package com.ccd.front.model.domain;

import lombok.Data;

/**
 * 返回字典封装类
 */
@Data
public class Dict {

    private String value;

    private String label;
}
