package com.ccd.front.model.domain;

import lombok.Data;

@Data
public class BlogCardType {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 博客卡片类型
     */
    private String cardType;

    /**
     * 博客卡片类型
     */
    private String typeName;

    /**
     * 博客卡片类型
     */
    private String isActive;

}
