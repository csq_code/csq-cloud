package com.ccd.front.constnt;

/**
 * 博客卡片类型常量
 *
 */
public class CardTypeConstants
{
    public static final String MUSIC = "music";

    public static final String VIDEO = "video";

    public static final String ARTICLE = "article";

}
