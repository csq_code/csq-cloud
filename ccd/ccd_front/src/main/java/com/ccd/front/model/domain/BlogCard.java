package com.ccd.front.model.domain;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class BlogCard<T> {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 序号，非数据库字段
     */
    private String itemIndex;

    /**
     * 博客卡片类型
     */
    private String cardType;

    /**
     * 卡片类型名称
     */
    private String typeName;

    /**
     * 博客卡片标题
     */
    private String cardTitle;

    /**
     * 博客创建时间
     */
    private Timestamp createTime;

    private String createTimeStr;

    /**
     * 是否发布
     */
    private String isPublished;

    /**
     * 发布时间
     */
    private Timestamp publishTime;


    /**
     * 博客卡片资源主键（需去对应表里查询）
     */
    private int resourceId;

    /**
     * 博客卡片内容，音乐、视频、文章等等
     */
    private T bolgContent;

}
