package com.ccd.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 文件模块调用
 * @author ccd
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class CcdFileApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(CcdFileApplication.class, args);
    }
}
