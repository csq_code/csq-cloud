# CsqCloud

### 介绍
青云个人网站-后端

### 软件架构

#### 后端
开发工具：IntelliJ IDEA 2021.3.1

springCoot版本 2.7.18

springCloud版本 2021.0.8

mySql: 8.0.27

redis: 3.0.504

nacos: 2.3.2 (单体启动命令如下，若启动失败，先尝试打开数据库并连上nacos配置库)
cd /d "D:\pro\nacos-server-2.3.2\nacos\bin"  
startup.cmd -m standalone

sentinel: sentinel-dashboard-1.8.8.jar

#### 前端
开发工具：HBuilder X

node版本：v18.20.2

vite版本：5.2.0

vue版本：3.4.21（组合式写法<script setup>）
