package com.ccd.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class CcdGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(CcdGatewayApplication.class, args);
    }

}
